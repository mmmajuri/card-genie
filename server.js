const express = require('express');
const app = express();
const fs = require('fs');
const http = require('http').Server(app);
const io = require('socket.io')(http);
const chalk = require('chalk');

const { parser } = require('./utils/cardParser');
const readModuleFile = require('./utils/readModuleFile');

const {
  PLAYER_JOINED,
  PLAYER_LEFT,
  JOINED_GAME,
  RECIEVE_CARDS,
  NEW_QM,
  SHOW_BLACK_CARD,
  GAME_WAITING_FOR_PLAYERS,
  START_ROUND,
  NEW_ANSWER_RECIEVED,
  SHOW_ANSWERS_FROM_ROUND,
  RECIEVE_ANSWERS,
  SHOW_WINNER,
  END_ROUND,
} = require('./types/socketMessages.type');

const {
  SERVER,
} = require('./types/machineStates.type');

const port = process.env.PORT || 8080;

let blackCardData, whiteCardData;

readModuleFile('../data/blackCards.txt', (err, data) => {
  if (err) throw err;
  blackCardData = data;
  game.blackCards = parser.parse(blackCardData, 'blackCards');
});

readModuleFile('../data/whiteCards.txt', (err, data) => {
  if (err) throw err;
  whiteCardData = data;
  game.whiteCards = parser.parse(whiteCardData, 'whiteCards');
});

const shuffle = function(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

const getPlayerIndex = (searchedId, players) => {
  return players
    .map(({ id }) => id)
    .indexOf(searchedId.id);
}

const addPlayer = function(player) {
  const { socket } = player;
  this.players.push(player);
  this.dispatch('TRY_START_ROUND');
  socket.emit(JOINED_GAME, { serverMachineState: this.state });
  socket.broadcast.emit(PLAYER_JOINED, {
    name: player.name,
    amountOfPlayers: this.players.length,
  });
};

const removePlayer = function(player) {
  let { players } = this;
  const removedPlayer =
    players
      .filter(({ id }) => id === player.id)[0];
  const newPlayers =
    players
      .filter(({ id }) => id !== player.id)

  if (this.qm === player.id) {
    this.qm = undefined;
    this.selectQM();
  }

  this.players = newPlayers;
  io.emit(PLAYER_LEFT, {
    name: removedPlayer ? removePlayer.name : null,
    amountOfPlayers: this.players.length,
    amountOfPlayersInRound: this.playersInGame().length,
  });
  if (this.playersInGame().length < 2) {
    this.transition(SERVER.WAITING_FOR_ENOUGH_PLAYERS);
  } else {
    this.dispatch('TRY_TO_TRANSITION');
  }
};

const dealCards = function(amount, to) {
  const shuffledCards = shuffle(this.whiteCards);
  if (shuffledCards.length < amount) {
    console.log(chalk.bold.red('ERR: too few cards'));
    return false;
  }
  const cardsToBeDealt = shuffledCards.splice(0, amount);
  to.emit(RECIEVE_CARDS, cardsToBeDealt);
  this.whiteCards = shuffledCards;
};

const selectQM = function() {
  let { qm } = this;
  const playersInGame = this.playersInGame();
  if (!qm) {
    qm = playersInGame[0];
  } else {
    const qmIndex = getPlayerIndex(qm, playersInGame);
    const nextIndex = playersInGame.length > qmIndex + 1 ? qmIndex + 1 : 0;
    qm = playersInGame[nextIndex];
  }
  this.qm = qm;
  qm.socket.emit(NEW_QM);
};

const showBlackCard = function() {
  const shuffledCards = shuffle(this.blackCards);
  this.activeBlackCard = shuffledCards.pop();
  io.emit(SHOW_BLACK_CARD, this.activeBlackCard);
  this.blackCards = shuffledCards;
  this.transition(SERVER.WAITING_FOR_ANSWERS);
};

const setPlayersForRound = function () {
  const readyPlayers =
    this.players
      .filter(({ ready }) => ready);

  readyPlayers.forEach(player => player.isInGame = true);
}

const game = {
  players: [],
  blackCards: [],
  whiteCards: [],
  activeBlackCard: undefined,
  usedBlackCards: [],
  answers: [],
  playersInGame: function() {
    return this.players
      .filter(({ isInGame }) => isInGame);
  },
  winner: undefined,
  qm: undefined,
  state: SERVER.WAITING_FOR_ENOUGH_PLAYERS,
  states: {
    'GLOBAL': {
      'ADD_PLAYER': addPlayer,
      'REMOVE_PLAYER': removePlayer,
    },
    [SERVER.WAITING_FOR_ENOUGH_PLAYERS]: {
      'ENTER_STATE': function() {
        io.emit(GAME_WAITING_FOR_PLAYERS);
      },
      'TRY_START_ROUND': function() {
        const readyPlayers =
          this.players
            .filter(({ ready }) => ready);

        if (readyPlayers.length > 1) {
          this.transition(SERVER.STARTING_ROUND);
        };
      },
    },
    [SERVER.STARTING_ROUND]: {
      'ENTER_STATE': function() {
        setPlayersForRound.apply(game);
        selectQM.apply(game);
        showBlackCard.apply(game);
        io.emit(START_ROUND, {
          amountOfPlayersInRound: this.playersInGame().length
        });
      },
    },
    [SERVER.WAITING_FOR_ANSWERS]: {
      [RECIEVE_ANSWERS]: function(answers, player) {
        const playersInGame = this.playersInGame();
        const playerIsInGame =
          playersInGame.find(({ id }) => id === player);

        if (playerIsInGame) {
          this.answers.push(answers);
          this.dispatch('TRY_TO_TRANSITION');
        }
      },
      'TRY_TO_TRANSITION': function() {
        if (this.answers.length === this.playersInGame().length) {
          this.transition(SERVER.READING_ANSWERS);
        }
      },
    },
    [SERVER.READING_ANSWERS]: {
      'ENTER_STATE': function() {
        io.emit(SHOW_ANSWERS_FROM_ROUND, this.answers);
        this.transition(SERVER.WAITING_FOR_WINNER_SELECTION);
      },
    },
    [SERVER.WAITING_FOR_WINNER_SELECTION]: {
      'SELECT_WINNER': function({ answer }) {
        this.winner = answer;
        this.transition(SERVER.SHOWING_WINNER);
      }
    },
    [SERVER.SHOWING_WINNER]: {
      'ENTER_STATE': function() {
        io.emit(SHOW_WINNER, this.winner);
        setTimeout(() => {
          this.transition(SERVER.ENDING_ROUND);
        }, 2000);
      },
      'EXIT_STATE': function() {
        io.emit(END_ROUND);
      },
    },
    [SERVER.ENDING_ROUND]: {
      'ENTER_STATE': function() {
        this.players.forEach((player) => {
          player.isInGame = false;
        });
        this.answers = [];
        this.activeBlackCard = undefined;
        this.winner = undefined;
        this.transition(SERVER.STARTING_ROUND);
      },
    },
  },
  dispatch(actionName, ...payload) {
    console.log(chalk.blue.bold('DISPATCH: '), chalk.green(actionName), payload);
    const actions = this.states[this.state];
    const action = actions[actionName] || this.states['GLOBAL'][actionName];

    if (action) {
      action.apply(game, payload);
    }
  },
  transition(newState) {
    console.log(chalk.yellow.bold('TRANSITION: '), newState);
    this.dispatch('EXIT_STATE');
    this.state = newState;
    this.dispatch('ENTER_STATE');
  },
};

app.use(express.static('./web/dist/'));

app.get('/', (req, res) => {
  res.sendFile('index.html');
});

io.on('connection', (socket) => {
  socket.on('ADD_PLAYER', (player) => {
    console.log(chalk.green.bold('ADD_PLAYER: ', socket.id, player.name));
    player.socket = socket;
    player.id = socket.id;
    player.ready = false;
    game.dispatch('ADD_PLAYER', player);
  });
  socket.on('PLAYER_READY', () => {
    console.log(chalk.green.bold('PLAYER_READY'));
    game.players.find(({ id }) => id === socket.id).ready = true;
    game.dispatch('TRY_START_ROUND');
  });
  socket.on('REQUEST_CARDS', (amount) => {
    console.log(chalk.green.bold('REQUEST_CARDS'));
    dealCards.apply(game, [amount, socket]);
  });
  socket.on('SEND_ANSWERS', (answers) => {
    console.log(chalk.green.bold('RECIEVE_ANSWERS: ', answers));
    game.dispatch(RECIEVE_ANSWERS, answers, socket.id);
  });
  socket.on('SELECT_WINNER', ({ answers }) => {
    game.dispatch('SELECT_WINNER', { selectedBy: socket.id, answer: answers });
  })
  socket.on('disconnect', () => {
    console.log(chalk.green.bold('DISCONNECT: ', socket.id));
    game.dispatch('REMOVE_PLAYER', { id: socket.id });
  });
});

http.listen(port, () => {
  console.log('Listening to %d', port);
});
