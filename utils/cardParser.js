const CARD_CONSTS = {
  TYPE_BLACK: 'blackCards',
  TYPE_WHITE: 'whiteCards',
};

function parseIntoCardObjArray(cardStr) {
  return cardStr
    .split('\n')
    .map((card, i) => ({ id: i, text: card }));
}

function parseBlackCards(cards) {
  return cards.map((card) => {
    const numberOfAnswers = card.text.match(/_+/g) ? card.text.match(/_+/g).length : 1;
    return Object.assign({}, card, { numberOfAnswers });
  });
}

module.exports.parser = {
  parse: (cardStr, type) => {
    const cardsArray = parseIntoCardObjArray(cardStr);
    let cards = cardsArray;
    if (type === CARD_CONSTS.TYPE_BLACK) {
      cards = parseBlackCards(cardsArray);
    }
    return cards;
  },
};
