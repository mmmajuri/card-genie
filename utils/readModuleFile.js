const fs = require('fs');

module.exports = (path, callback) => {
  try {
    const filename = require.resolve(path);
    fs.readFile(filename, 'utf8', callback);
  } catch (e) {
    callback(e);
  }
}
