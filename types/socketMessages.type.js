module.exports = {
  PLAYER_JOINED: 'PLAYER_JOINED',
  PLAYER_LEFT: 'PLAYER_LEFT',
  JOINED_GAME: 'JOINED_GAME',
  RECIEVE_CARDS: 'RECIEVE_CARDS',
  SEND_ANSWERS: 'SEND_ANSWERS',
  NEW_QM: 'NEW_QM',
  SHOW_BLACK_CARD: 'SHOW_BLACK_CARD',
  GAME_WAITING_FOR_PLAYERS: 'GAME_WAITING_FOR_PLAYERS',
  START_ROUND: 'START_ROUND',
  NEW_ANSWER_RECIEVED: 'NEW_ANSWER_RECIEVED',
  RECIEVE_ANSWERS: 'RECIEVE_ANSWERS',
  SHOW_WINNER: 'SHOW_WINNER',
  END_ROUND: 'END_ROUND',
  ADD_PLAYER: 'ADD_PLAYER',
  REQUEST_CARDS: 'REQUEST_CARDS',
  SEND_ANSWERS: 'SEND_ANSWERS',
  SHOW_ANSWERS_FROM_ROUND: 'SHOW_ANSWERS_FROM_ROUND',
  SELECT_WINNER: 'SELECT_WINNER',
}
