import Vue from 'vue';
import VueObserveVisibility from 'vue-observe-visibility';
import VueSocketio from 'services/vue-socket.io';
import io from 'socket.io-client';

import App from './App.vue';
import store from './store/store';

const socketAddress = process.env.NODE_ENV === 'development' ? 'http://localhost:8080/' : `:${process.env.PORT}`;

const socket = io(socketAddress);
window.socket = socket;

Vue.config.productionTip = false;

Vue.use(VueObserveVisibility);
Vue.use(VueSocketio, socket, store);

new Vue({
  store,
  render: h => h(App),
}).$mount('#app');
