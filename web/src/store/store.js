import Vue from 'vue';
import Vuex from 'vuex';
import chalk from 'chalk';

import hand from './modules/hand';
import game from './modules/game';
import gameboard from './modules/gameboard';
import player from './modules/player';
import machineStateStore from './modules/machineStateStore';

Vue.use(Vuex);

const logger = store => {
  store.subscribe((mutation) => {
    console.log(chalk.blue(mutation.type), mutation);
  });
  store.subscribeAction((action) => {
    console.log(chalk.green(action.type), action);
  });
}

export default new Vuex.Store({
  modules: {
    hand,
    game,
    gameboard,
    player,
    machineStateStore,
  },
  plugins: [ logger ],
})
