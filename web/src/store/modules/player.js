import {
  PLAYER,
} from 'types/actions.type';

const {
  SET_NAME,
  // INCREASE_SCORE,
  // DECREASE_SCORE,
  // SET_IS_IN_ROUND,
} = PLAYER;

import {

} from 'types/mutations.type';

import {
  ADD_PLAYER,
} from 'types/socketMessages.type';

const initialState = {
  name: undefined,
  score: 0,
  isInRound: false,
};

export const state = Object.assign({}, initialState);

export const actions = {
  [SET_NAME] (context, { name }) {
    context.commit(SET_NAME, { name });
    window.socket.emit(ADD_PLAYER, { name });
  },
};

export const mutations = {
  [SET_NAME] (state, { name }) {
    state.name = name;
  },
};

export const getters = {
  name({ name }) {
    return name;
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
};
