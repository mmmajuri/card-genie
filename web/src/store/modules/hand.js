import {
  SOCKET,
  HAND,
} from 'types/actions.type';

import {
  SET_CARDS,
} from 'types/mutations.type';

import {
  REQUEST_CARDS
} from 'types/socketMessages.type';

import {
  IDLE,
} from 'types/machineStates.type';

const initialState = {
  // TODO create rules config to host card amounts etc.
  fullHandCardAmount: 5,
  cards: [],
  machineState: IDLE,
};

export const state = Object.assign({}, initialState);

export const getters = {
  cards({ cards }) {
    return cards;
  },
  fullHandCardAmount({ fullHandCardAmount }) {
    return fullHandCardAmount;
  },
};

export const mutations = {
  [SET_CARDS] (state, cards) {
    state.cards = cards;
  },
};

export const actions = {
  [HAND.ADD_CARD] (context, card) {
    const newCards = context.getters.cards.concat(card);
    context.commit(SET_CARDS, newCards);
  },
  [HAND.REMOVE_CARD] (context, card) {
    const newCards =
      context.getters.cards
        .filter(({ id }) => id !== card.id);

    context.commit(SET_CARDS, newCards);
  },
  [SOCKET.JOINED_GAME] ({ getters }) {
    window.socket.emit(REQUEST_CARDS, getters.fullHandCardAmount);
  },
  [SOCKET.END_ROUND] ({ getters }) {
    const cardsNeeded = getters.fullHandCardAmount - getters.cards.length;
    window.socket.emit(REQUEST_CARDS, cardsNeeded);
  },
  [SOCKET.RECIEVE_CARDS] (context, cards) {
    for (let i = 0; i < cards.length; i++) {
      const card = cards[i];
      context.dispatch(HAND.ADD_CARD, card);
    }
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
}
