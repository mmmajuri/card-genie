import {
  RESET_MACHINE_STATE,
} from 'types/actions.type';

import {
  SET_MACHINE_STATE,
} from 'types/mutations.type';

import {
  GAME,
  APP,
  GAMEBOARD,
} from 'types/machineStates.type';

const initialState = {
  APP: APP.SETUP,
  GAME: GAME.NOT_PLAYING,
  GAMEBOARD: GAMEBOARD.NOT_READY_TO_GIVE_ANSWERS,
};

export const state = Object.assign({}, initialState);

export const actions = {
  [RESET_MACHINE_STATE] (context) {
    context.commit(SET_MACHINE_STATE, { machineState: initialState.machineState });
  },
};

export const mutations = {
  [SET_MACHINE_STATE] (state, { targetMachine, value }) {
    state[targetMachine] = value;
  },
};

export const getters = {
  machineState(state) {
    return state;
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
}
