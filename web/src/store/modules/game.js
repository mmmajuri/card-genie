import {

} from 'types/actions.type';

import {

} from 'types/mutations.type';

import {
  IDLE,
} from 'types/machineStates.type'

const initialState = {
  players: [],
  machineState: IDLE,
};

export const state = Object.assign({}, initialState);

export const actions = {

};

export const mutations = {

};

export const getters = {

};

export default {
  state,
  actions,
  mutations,
  getters,
}
