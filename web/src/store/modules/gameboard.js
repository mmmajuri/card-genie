import {
  CARD_CLICKED,
  ADD_TO_ANSWERS,
  SOCKET,
  SELECTED_ANSWER_CLICKED,
} from 'types/actions.type';

import {
  SET_BLACK_CARD,
  SET_ANSWERS_IN_ROUND,
  ADD_SELECTED_ANSWER,
  REMOVE_SELECTED_ANSWER,
  SET_WINNER_OF_ROUND,
  SET_CURRENT_ANSWER_INDEX,
  SET_MACHINE_STATE,
  SET_SELECTED_ANSWERS,
} from 'types/mutations.type';

import {
  GAMEBOARD,
  GAME,
} from 'types/machineStates.type'

const initialState = {
  blackCard: undefined,
  answersInRound: [],
  winnerOfRound: undefined,
  selectedAnswers: [],
  currentAnswerIndex: 0,
};

export const state = Object.assign({}, initialState);

export const actions = {
  [CARD_CLICKED] (context, { card }) {
    context.dispatch(ADD_TO_ANSWERS, { card });
  },
  [SELECTED_ANSWER_CLICKED] (context, { id }) {
    context.commit(REMOVE_SELECTED_ANSWER, { idToBeRemoved: id });
    context.commit(SET_MACHINE_STATE, {
        machineState: GAMEBOARD.NOT_READY_TO_GIVE_ANSWERS,
      });
  },
  [ADD_TO_ANSWERS] (context, { card }) {
    const { blackCard, selectedAnswers, machineState } = context.getters;
    const answersNeeded = blackCard[0].numberOfAnswers;

    if (machineState.GAMEBOARD === GAMEBOARD.READY_TO_GIVE_ANSWERS) {
      const lastAnswer = selectedAnswers[selectedAnswers.length - 1];
      context.commit(REMOVE_SELECTED_ANSWER, { idToBeRemoved: lastAnswer.id });
    }
    context.commit(ADD_SELECTED_ANSWER, { answer: card });

    if (selectedAnswers.length === answersNeeded) {
      context.commit(SET_MACHINE_STATE, {
          targetMachine: 'GAMEBOARD',
          value: GAMEBOARD.READY_TO_GIVE_ANSWERS
        });
    }
  },
  [SOCKET.SHOW_BLACK_CARD] (context, blackCard) {
    context.commit(SET_BLACK_CARD, { blackCard });
  },
  [SOCKET.SHOW_ANSWERS_FROM_ROUND] (context, answers) {
    context.commit(SET_ANSWERS_IN_ROUND, { answers: answers[0] });
    context.commit(SET_MACHINE_STATE, {
      targetMachine: 'GAMEBOARD',
      value: GAMEBOARD.SHOWING_ANSWERS_FROM_ROUND,
    });
  },
  [SOCKET.SHOW_WINNER] (context, winner) {
    context.commit(SET_WINNER_OF_ROUND,  winner[0]);
    context.commit(SET_MACHINE_STATE, {
      targetMachine: 'GAME',
      value: GAME.WAITING_FOR_ROUND_END,
    });
  },
};

export const mutations = {
  [SET_BLACK_CARD] (state, { blackCard }) {
    state.blackCard = blackCard;
  },
  [SET_ANSWERS_IN_ROUND] (state, { answers }) {
    state.answersInRound = answers;
  },
  [ADD_SELECTED_ANSWER] (state, { answer }) {
    state.selectedAnswers.push(answer);
  },
  [REMOVE_SELECTED_ANSWER] (state, { idToBeRemoved }) {
    const newAnswers =
      state.selectedAnswers
        .filter(({ id }) => id !== idToBeRemoved);
    state.selectedAnswers = newAnswers;
  },
  [SET_SELECTED_ANSWERS] (state, selectedAnswers) {
    state.selectedAnswers = selectedAnswers;
  },
  [SET_WINNER_OF_ROUND] (state, winnerOfRound) {
    state.winnerOfRound = winnerOfRound;
  },
  [SET_CURRENT_ANSWER_INDEX] (state, { currentAnswerIndex }) {
    state.currentAnswerIndex = currentAnswerIndex;
  },
};

export const getters = {
  blackCard({ blackCard }) {
    return blackCard;
  },
  answersInRound({ answersInRound }) {
    return answersInRound;
  },
  selectedAnswers({ selectedAnswers }) {
    return selectedAnswers;
  },
  winnerOfRound({ winnerOfRound }) {
    return winnerOfRound;
  },
  currentAnswerIndex({ currentAnswerIndex }) {
    return currentAnswerIndex;
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
}
