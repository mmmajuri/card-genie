const path = require('path');

module.exports = {
  lintOnSave: true,
  configureWebpack: {
    resolve: {
      alias: {
        types: path.resolve(__dirname, '../types/'),
        services: path.resolve(__dirname, './src/services/'),
      },
    },
  },
}
